#Labo 1

## Question 1

Expliquer les attributs:

##### match_parent
> Anciennement FILL_PARENT (aux version inférieures à l'API niveau 8), le match_parent est une valeur spéciale pour la hauteur ou la largeur demandée par une vue. Cette vue peut-être plus grande que son parent, moins le padding du parent s'il en a un. (Pour info, le padding est une propriété qui permet de définir les différents écarts de remplissage sur les quatres côtés d'un élément)

##### wrap_content
> Le wrap_content est une valeur spéciale pour la hauteur ou la largeur demandée par une vue. Il signifie que la vue doit être juste assez grande pour s'adapter à son propre contenu interne, en tenant compte de son propre padding

##### android:orientation
> Spécifie si les vues enfant sont affichées en ligne ou en colonne, La mise en page doit-elle être en colonne ou en ligne ? Utilisez "horizontal" pour une ligne, "vertical" pour une colonne. 

##### android:text
> C'est le texte à afficher. Il peut s'agir d'une valeur de chaîne, utilisant "\\ ;" pour échapper à des caractères tels que "\n" ou "\\uxxxx" pour un caractère unicode ;

## Question 2

##### 1. Quel est le package de la classe ~~Button
> On retrouve la classe button dans "android.widget.Button". Les widgets sont des applications utilisables directement sur le menu principal du téléphone 

##### 2. Donnez 5 autres classes de ce package qui vous sont familières, par exemple des classes proches des classes Javafx.
> ListView, RadioButton, ImageView, CheckBox, ProgressBar

##Question 3
##### 1.Dans quelle classe est déﬁnie la méthode findViewById ?
> Cette classe est définie dans AppCompatActivity

##### 2.Quel est son type de retour ?
> Son type de retour est un "int". On récupère donc un ID de type int

## Question 4
##### 1.Quel type d’élément est View.OnClickListener, une classe, une méthode, un constructeur, ou autre chose ?
> C'est une interface pour un callback à invoquer lors d'un clic sur une vue

##### 2.Dans quel package se trouve cet élément ?
> On le retrouve dans le package android.view.View.OnClickListener

## Question 5
##### 1.Quel est le package de la classe Log ?
> Android.util.Log

##### 2.Quelles sont les méthodes disponibles dans la classe Log ?
> static int d(String tag, String msg, Throwable tr)
>  Envoyez un message de journal DEBUG et enregistrez l'exception dans le journal.
>  
>  static int d(String tag, String msg)
>  Envoyez un message de journal DEBUG.
>  
>  static int e(String tag, String msg)
>  Envoyez un message de journal d'erreurs.
>  
>  static int e(String tag, String msg, Throwable tr)
>  Envoyez un message de journal d'erreurs et enregistrez l'exception dans le journal.
>  
>  static String getStackTraceString(Throwable tr)
>  Fonction pratique pour obtenir la trace d'une pile enregistrable à partir d'un Throwable
>  
>  static int i(String tag, String msg, Throwable tr)
>  Envoyez un message INFO log et enregistrez l'exception dans le journal.
>  
>  static int i(String tag, String msg)
>  Envoyez un message INFO log.
>  
>  static boolean isLoggable(String tag, int level)
>  Vérifie si un journal pour la balise spécifiée est journalisable ou non au niveau spécifié.
>  
>  static int println(int priority, String tag, String msg)
>  Appel d'enregistrement de bas niveau.
>  
>  static int v(String tag, String msg)
>  Envoyez un message de journal VERBOSE.
>  
>  static int v(String tag, String msg, Throwable tr)
>  Envoyez un message de journal VERBOSE et enregistrez l'exception.
>  
>  static int w(String tag, Throwable tr)
>  Envoyez un message de journal WARN et enregistrez l'exception.
>  
>  static int w(String tag, String msg, Throwable tr)
>  Send a WARN log message and log the exception.
>  
>  static int w(String tag, String msg)
>  Send a WARN log message.
>  
>  static int wtf(String tag, String msg)
>  What a Terrible Failure: Report a condition that should never happen.
>  
>  static int wtf(String tag, Throwable tr)
>  What a Terrible Failure: Report an exception that should never happen.
>  
>  static int wtf(String tag, String msg, Throwable tr)
>  What a Terrible Failure: Report an exception that should never happen.


#Labo 2

## Question 1

Quelles méthodes de callback sont appelées lorsque: 

##### 1. Vous lancez l'application
> onCreate et onStart

##### 2. Vous quittez l'application (bouton back après avoir lancé)
> onStop et onDestroy

##### 3. Vous changez d'application (avec le task manager)
>onPause onStop 

##### 4. Vous revenez à l'application (avec le task manager)
> onRestart, onStart et onResume

##### 5. Vous effectuez une rotation de votre appareil
> onPause, onStop, onDestroy, onCreate, onStart, onResume