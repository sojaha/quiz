package be.abaron.quizz.models;

import java.io.Serializable;
import java.util.Random;

import be.abaron.quizz.R;

public class Quiz implements Serializable {

    private  Question[] mQuestionBank;

    private int mCurrentIndex;
    Random rand = new Random();

    public Quiz() {
        generateQuestion();
        shuffleQuestion();
        mCurrentIndex = 0;
    }

    public void generateQuestion() {
        mQuestionBank = new Question[] {
                new Question(R.string.question_australia, true),
                new Question(R.string.question_oceans, true),
                new Question(R.string.question_mideast, false),
                new Question(R.string.question_csharp, false),
                new Question(R.string.question_java, false),
                new Question(R.string.question_html, true),
        };
    }

    public void shuffleQuestion() {
        for (int i = 0; i < mQuestionBank.length; i++) {
            int randomIndexToSwap = rand.nextInt(mQuestionBank.length);
            Question tmp = mQuestionBank[randomIndexToSwap];
            mQuestionBank[randomIndexToSwap] = mQuestionBank[i];
            mQuestionBank[i] = tmp;
        }
    }

    public Question getQuestion() {
        if (mCurrentIndex == mQuestionBank.length) {
            mCurrentIndex = 0;
        }
        return mQuestionBank[mCurrentIndex];
    }

    public void nextQuestion() {
        if (mCurrentIndex < mQuestionBank.length) {
            mCurrentIndex = mCurrentIndex + 1;
        } else {
            mCurrentIndex = 0;
        }
    }

    public int getCurrentIndex() {
        return mCurrentIndex;
    }

    public void setCurrentIndex(int mCurrentIndex) {
        this.mCurrentIndex = mCurrentIndex;
    }
}
