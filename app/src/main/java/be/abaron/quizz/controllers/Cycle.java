package be.abaron.quizz.controllers;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class Cycle extends AppCompatActivity {
    private static final String TAG = "Cycle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate method");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart method");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume method");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause method");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop method");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart method");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy method");
    }
}
