package be.abaron.quizz.controllers;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import be.abaron.quizz.R;

public class CheatActivity extends AppCompatActivity {

    public static final String ANSWER_EXTRA = "ANSWER_EXTRA";
    public static final String EXTRA_ANSWER_SHOWN = "EXTRA_ANSWER_SHOWN";
    private boolean mAnswerTrue;

    private Button mCheatButton;
    private TextView mAnswerTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        mAnswerTrue = getIntent().getBooleanExtra(ANSWER_EXTRA, false);

        mAnswerTextView = findViewById(R.id.answer_text_view);
        mCheatButton = findViewById(R.id.show_answer_button);

        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAnswerTextView.setText(mAnswerTrue ? R.string.true_button
                        : R.string.false_button);
                mAnswerTextView.setVisibility(View.VISIBLE);
                Intent data = new Intent();
                data.putExtra(EXTRA_ANSWER_SHOWN, true);
                setResult(RESULT_OK, data);
            }
        });

    }
}