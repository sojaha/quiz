package be.abaron.quizz.controllers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import be.abaron.quizz.R;
import be.abaron.quizz.models.Quiz;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final String SCORE_INDEX = "score";
    private static final String QUESTION_INDEX = "question";
    private static final int REQUEST_CODE = 1234; //Constante à définir pour l'intent
    Button mTrueButton; //préfixe m = convention android pour designer les attributs non publics et non statiques d'un classe
    Button mFalseButton;
    Button mNextButton;
    Button mEndButton;
    Button mCheatButton;
    TextView mQuestionTextView;
    TextView mScoreTextView;
    int mScoreCount = 0;
    Quiz mQuiz;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Initialisation des attributs
        mQuiz = new Quiz();
        mTrueButton = findViewById(R.id.true_button); //On doit accéder à la constante true_button de la classe statique id qui est une classe imbriquée à la classe R
        mFalseButton = findViewById(R.id.false_button);
        mNextButton = findViewById(R.id.next_button);
        mEndButton = findViewById(R.id.end_button);
        mCheatButton = findViewById(R.id.cheat_button);
        mQuestionTextView = findViewById(R.id.question_textView);
        mQuestionTextView.setText(mQuiz.getQuestion().getTextResId());
        mScoreTextView = findViewById(R.id.score_textView);
        mScoreTextView.setText("Score: " + mScoreCount);

        //Ajout d'un listener (ecouteur) aux buttons
        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MainActivity", "Clique sur le bouton Vrai"); //visible dans la console Logcat d'android studio (en bas à gauche)
                choice(mQuiz.getQuestion().isAnswerTrue());
            }
        });

        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MainActivity", "Clique sur le bouton Faux"); //visible dans la console Logcat d'android studio (en bas à gauche)
                choice(!mQuiz.getQuestion().isAnswerTrue());
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MainActivity", "Clique sur le bouton Suivant"); //visible dans la console Logcat d'android studio (en bas à gauche)
                mQuiz.nextQuestion();
                mQuestionTextView.setText(mQuiz.getQuestion().getTextResId());
            }
        });

        mEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MainActivity", "Clique sur le bouton Terminer"); //visible dans la console Logcat d'android studio (en bas à gauche)
                Toast.makeText(MainActivity.this,
                        R.string.endGame, Toast.LENGTH_SHORT).show();
                mScoreCount = 0;
                mScoreTextView.setText("Score: " + mScoreCount);
            }
        });

        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MainActivity", "Clique sur le bouton Cheat"); //visible dans la console Logcat d'android studio (en bas à gauche)
                Toast.makeText(MainActivity.this,
                        R.string.cheatToast, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, CheatActivity.class);
                intent.putExtra(CheatActivity.ANSWER_EXTRA,
                        mQuiz.getQuestion().isAnswerTrue());
                startActivityForResult(intent, REQUEST_CODE); //Va appeler l'ActivityManager de l'appareil et se chargera de lancer l'activité
            }
        });

        if (savedInstanceState != null) {
            Log.i(TAG, "RECUPERATION");
            mScoreCount = savedInstanceState.getInt(SCORE_INDEX);
            mScoreTextView.setText("Score: " + mScoreCount);
            //mQuiz = (Quiz) savedInstanceState.getSerializable(QUESTION_INDEX);
            //mQuestionTextView.setText(Objects.requireNonNull(mQuiz).getQuestion().getTextResId());
        }
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState");
        outState.putInt(SCORE_INDEX, mScoreCount);
        //outState.putSerializable(QUESTION_INDEX, mQuiz);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CheatActivity.RESULT_OK) {
            if (data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN, false)) {
                Toast.makeText(
                        this, getString(R.string.answer_has_been_shown) + " " +
                                    mQuiz.getQuestion().isAnswerTrue(),
                                Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void choice(boolean answerTrue) {
        if (answerTrue) {
            Toast.makeText(MainActivity.this,
                    R.string.goodAnswer, Toast.LENGTH_SHORT).show();
            mQuiz.nextQuestion();
            mQuestionTextView.setText(mQuiz.getQuestion().getTextResId());
            mScoreCount++;
            mScoreTextView.setText("Score: " + mScoreCount);
        } else {
            Toast.makeText(MainActivity.this,
                    R.string.wrongAnswer, Toast.LENGTH_SHORT).show();
        }
    }

}